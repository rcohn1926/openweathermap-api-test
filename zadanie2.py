from datetime import datetime
import json

with open('parsed_data.json', 'r') as f:
    data = json.load(f)

delta_max = -1
delta_max_pos = [0, 0]
for i in range(0, len(data)):
    for j in range(0, len(data[i]['hourly_data'])-1):
        delta_tmp = abs(data[i]['hourly_data'][j+1]['temp'] - data[i]['hourly_data'][j]['temp'])
        if delta_tmp > delta_max:
            delta_max = delta_tmp
            delta_max_pos = [i, j]
beg_time = datetime.fromtimestamp(data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]]['dt']).isoformat() + 'Z'
end_time = datetime.fromtimestamp(data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]+1]['dt']).isoformat() + 'Z'
beg_temp = data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]]['temp']
end_temp = data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]+1]['temp']
mean_hum = (
                   data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]]['humidity'] +
                   data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]+1]['humidity']) / 2
mean_press = (
                     data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]]['pressure'] +
                     data[delta_max_pos[0]]['hourly_data'][delta_max_pos[1]+1]['pressure']) / 2

print("Okres największego wzrostu:", beg_time, "-", end_time)
print("Skok temperatury:", delta_max, "\u2103. Z temperatury:", beg_temp, "\u2103, o godzinie:", beg_time)
print("Średnia wilgotność w tym okresie:", mean_hum, "%. Średnie ciśnienie wyniosło:", mean_press, "hPa.")
