import time
import requests
import json

# deklaracje zmiennych
api_key = "e3c1568f43f962dcc0ac857a18ca5d4a"  # klucz API, żeby dostać się do "Historical weather 5 days"
# lat i lon to współrzędne (Warszawa), można znaleźć w dokumentacji API
lat = "53.13"
lon = "21"
time = int(time.time())  # pobieramy obecny czas
day_in_sec = 86400  # długość dnia w sekundach
days_back = 3  # ilość dni o które się cofamy
time -= day_in_sec * days_back
ddata = []

# pobranie i agregacja danych z trzech poprzednich dni
for i in range(0, days_back):
    url = "https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=%s&lon=%s&dt=%s&units=metric&appid=%s"\
          % (lat, lon, time, api_key)
    response = requests.get(url)
    response_data = json.loads(response.text)
    ddata += response_data['hourly']
    time += day_in_sec

current_length = 0
start_pos = 100
parsed_data = []
for i in range(0, len(ddata) - 1):
    if ddata[i]['temp'] < ddata[i+1]['temp']:
        if current_length == 0:
            start_pos = i
            current_length += 1
        else:
            current_length += 1
    else:
        if current_length != 0:
            dict_temp = {
                "start_time": ddata[start_pos]['dt'],
                "duration": current_length,
                "temp_start": ddata[start_pos]['temp'],
                "temp_end": ddata[start_pos + current_length]['temp'],
                "hourly_data": ddata[start_pos:(start_pos + current_length + 1)]
            }
            parsed_data.append(dict_temp)
            current_length = 0

with open('parsed_data.json', 'w') as f:
    json.dump(parsed_data, f, indent=4)
